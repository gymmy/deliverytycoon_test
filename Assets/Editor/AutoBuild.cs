﻿using System;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

public class AutoBuild {
    private static string[] GetScenes() {
        List<string> temp = new List<string>();
        foreach (EditorBuildSettingsScene s in EditorBuildSettings.scenes) {
            if (s.enabled) {
                temp.Add(s.path);
            }
        }

        return temp.ToArray();
    }

	public static void BuildAssetBundle() {
		string folder = "";
		switch (EditorUserBuildSettings.activeBuildTarget) {
			case UnityEditor.BuildTarget.StandaloneWindows64:
				folder = "StandaloneWindows64";
				break;
			case UnityEditor.BuildTarget.Android:
				folder = "Android";
				break;
			case UnityEditor.BuildTarget.iOS:
				folder = "iOS";
				break;
		}

		string path = "Assets/StreamingAssets/AssetBundle/" + folder;
		bool dirExist = Directory.Exists(path);
		if (!dirExist) {
			Directory.CreateDirectory(path);
		}

		BuildPipeline.BuildAssetBundles(path, UnityEditor.BuildAssetBundleOptions.None, EditorUserBuildSettings.activeBuildTarget);
	}

	private static void GenerateApk(bool isJenkins, bool buildAndRun = false) {
        PlayerSettings.productName = "Delivery Tycoon";

        string version = GetArg("-version");
        if (!string.IsNullOrEmpty(version) && !version.Equals("null")) {
            PlayerSettings.bundleVersion = version;
        }

        PlayerSettings.Android.keystoreName = "../Keystore/keystore.keystore";
        PlayerSettings.Android.keystorePass = "otherocean";
        PlayerSettings.Android.keyaliasPass = "otherocean";

        string defines = string.Empty;
        bool debug = false;

        //switch (serverType) {
        //    case DTConstants.ServerType.Prod:
        //        defines = "IS_PROD;";
        //        break;
        //    case DTConstants.ServerType.QA:
        //        defines = "IS_QA;";
        //        defines += "DEBUG_ENABLE";
        //        debug = true;
        //        break;
        //    case DTConstants.ServerType.Dev:
        //        defines = "IS_DEV;";
        //        defines += "DEBUG_ENABLE";
        //        debug = true;
        //        break;
        //}

        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, defines);

        string directory = "Builds";

        if (!Directory.Exists(directory)) {
            Directory.CreateDirectory(directory);
        }

        string fileName = null;
        string buildPath = null;

        #region Naming
        if (isJenkins) {
            fileName = "dt";
            buildPath = directory + "/" + fileName + ".apk";
        } else {
            fileName = "dt";
            buildPath = directory + "/" + fileName + ".apk";
        }
        #endregion

        BuildOptions options = BuildOptions.None;
        if (debug) {
            options = BuildOptions.Development;
        }
        if (!isJenkins) {
            options = BuildOptions.ShowBuiltPlayer;
        }
        if (buildAndRun) {
            options |= BuildOptions.AutoRunPlayer;
        }

        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);

		bool rebuild = (GetArg("-rebuild") == "true") ? true : false;
		if(rebuild) {
			System.Console.WriteLine("S6LogJenkins: Asset Bundle exporter rebuild started. \n");
			BuildAssetBundle();
			//string[] abContent = abManifest.GetAllAssetBundles();	
			//foreach(string abc in abContent) {
			//	System.Console.WriteLine("S6LogJenkins: Asset '" + abc + "' included in AssetBundle.");
			//}
		}
		else {
			System.Console.WriteLine("S6LogJenkins: Asset Bundle exporter skipped. \n");
		}

		PlayerSettings.applicationIdentifier = "com.secret6.dtycoon"; 

		UnityEditor.Build.Reporting.BuildReport buildReport = BuildPipeline.BuildPlayer(GetScenes(), buildPath, BuildTarget.Android, options);
        if (buildReport.summary.result == UnityEditor.Build.Reporting.BuildResult.Failed) {
			// Open the file to read from.

			System.Console.WriteLine("S6LogJenkins: BuildPlayer failed! Logging build report. \n");

			UnityEditor.Build.Reporting.BuildStep[] steps = buildReport.steps;

			// Log BuildSteps
			foreach (UnityEditor.Build.Reporting.BuildStep step in steps) {
				System.Console.WriteLine("S6LogJenkins: BUILDSTEPNAME:" + step.name);

				// Build Step Messages
				foreach (UnityEditor.Build.Reporting.BuildStepMessage msg in step.messages) {
					System.Console.WriteLine("S6LogJenkins: BUILDSTEPMSG:" + msg.content);
				}
			}

			throw new Exception("S6LogJenkins: BuildPlayer failure");
        }
    }

    //[MenuItem("Delivery Tycoon/Generate Debug APK", false, 0)]
    //public static void GenerateDebugApk() {
    //    GenerateApk(false, DTConstants.ServerType.Dev);
    //}

    //[MenuItem("Delivery Tycoon/Generate Staging APK", false, 1)]
    //public static void GenerateStagingApk() {
    //    GenerateApk(false, DTConstants.ServerType.QA);
    //}

    [MenuItem("Delivery Tycoon/Generate Release APK", false, 2)]
    public static void GenerateReleaseApk() {
        GenerateApk(false);
    }

    //[MenuItem("Delivery Tycoon/Build and Run Non-system Debug APK", false, 30)]
    //public static void BuildRunNonSystemDebugApk() {
    //    bool proceed = EditorUtility.DisplayDialog(
    //        "Build and Run Debug APK",
    //        "Make sure you have:\n\n" +
    //        "1. Set C.java IS_SYSTEM_BUILD to false in native code.\n" +
    //        "2. Removed android:sharedUserId in Android Manifest.",
    //        "Okay", "Abort");
    //    if (proceed) {
    //        GenerateApk(false, DTConstants.ServerType.Dev, true);
    //    }
    //}

    //public static void GenerateJenkinsDebugApk() {
    //    GenerateApk(true, DTConstants.ServerType.Dev);
    //}

    //public static void GenerateJenkinsStagingApk() {
    //    GenerateApk(true, DTConstants.ServerType.QA);
    //}

    //public static void GenerateJenkinsReleaseApk() {
    //    GenerateApk(true, DTConstants.ServerType.Prod);
    //}

    private static bool ProceedBuildingProject(string path) {
        if (string.IsNullOrEmpty(path)) {
            UnityEngine.Debug.LogError("Invalid path folder.");
            return false;
        }

        bool proceed = EditorUtility.DisplayDialog("Build Project",
            "Save project at " + path + " ?",
            "Ok", "Cancel");
        if (proceed == false) {
            UnityEngine.Debug.Log("Cancel saving.");
            return false;
        }

        return true;
    }

    private static string GetArg(string name) {
        string[] args = Environment.GetCommandLineArgs();
        for (int i = 0; i < args.Length; i++) {
            if (args[i] == name && args.Length > i + 1) {
                return args[i + 1];
            }
        }
        return null;
    }

    public static void GenerateXcodeProject() {
        string version = GetArg("-version");
        if (!string.IsNullOrEmpty(version) && !version.Equals("null")) {
            PlayerSettings.bundleVersion = version;
        }

        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = GetScenes();
        buildPlayerOptions.locationPathName = "AutoBuild/iOS";
        buildPlayerOptions.target = BuildTarget.iOS;
        buildPlayerOptions.options = BuildOptions.None;

		bool rebuild = (GetArg("-rebuild") == "true") ? true : false;
		if (rebuild) {
			//BuildAssetBundle(BuildTarget.iOS);
		}

		UnityEditor.Build.Reporting.BuildReport buildReport = BuildPipeline.BuildPlayer(buildPlayerOptions);
        if (buildReport.summary.result == UnityEditor.Build.Reporting.BuildResult.Failed) {
            // Open the file to read from.
            throw new Exception("BuildPlayer failure");
        }
    }
}
